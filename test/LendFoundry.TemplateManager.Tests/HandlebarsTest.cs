﻿using System;
using Microsoft.AspNetCore.Builder;
using Moq;
using Xunit;
using HandlebarsTemplateProcessor1 = LendFoundry.TemplateManager.HandlebarsTemplateProcessor;

namespace LendFoundry.TemplateManager.Tests
{
    public class HandlebarsTest
    {
        [Fact]
        public void StartingHandlerBarsListenerExtensions()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                var handlebarResolver = new HandlebarsResolver();
                handlebarResolver.Start();

                var applicationBuilder = new Mock<IApplicationBuilder>();
                var serviceProvider = new Mock<IServiceProvider>();

                serviceProvider.Setup(x => x.GetRequiredService<IHandlebarsResolver>())
                               .Returns(handlebarResolver);

                applicationBuilder.Setup(x => x.ApplicationServices)
                                  .Returns(serviceProvider.Object);

                HandlebarsExtendedMapExtensions.UseHandlebars(applicationBuilder.Object);
            });
        }

        [Fact]
        public void HandlebarsTemplateProcessor_Process_Ok()
        {
            ITemplateProcessor templateProcessor = new HandlebarsTemplateProcessor1();
            var result = templateProcessor.Process("test", new { name = "vk" });
            Assert.Equal("test", result);
        }
    }

    public interface IServiceProvider : System.IServiceProvider
    {
        T GetService<T>() where T : class;

        T GetRequiredService<T>() where T : class;
    }
}
