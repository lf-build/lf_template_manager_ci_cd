﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Services;
using Moq;
using Xunit;

namespace LendFoundry.TemplateManager.Tests
{
    public class TemplateManagerServiceTests
    {
        private Mock<ITemplateManagerRepository> Repository { get; }
        private Mock<ITemplateProcessor> Processor { get; }
        private Mock<IEventHubClient> Event { get; }
        private ITemplateManagerService Service { get; }

        public TemplateManagerServiceTests()
        {
            Repository = new Mock<ITemplateManagerRepository>();
            Processor = new Mock<ITemplateProcessor>();
            Event = new Mock<IEventHubClient>();
            Service = new TemplateManagerService(Repository.Object, Processor.Object, Event.Object);
        }

        [Fact]
        public void TemplateManagerService_Constructor_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new TemplateManagerService(null, Mock.Of<ITemplateProcessor>(), Mock.Of<IEventHubClient>()));
            Assert.Throws<ArgumentNullException>(() => new TemplateManagerService(Mock.Of<ITemplateManagerRepository>(), Mock.Of<ITemplateProcessor>(), null));
        }

        [Fact]
        public async void TemplateManagerService_Get_WithEmptyOrNullTemplateName_ThrowsArgumentException()
        {
            await Assert.ThrowsAsync<ArgumentException>(() => Service.Get(string.Empty, null, Format.Text));
            await Assert.ThrowsAsync<ArgumentException>(() => Service.Get("test", null, Format.Text));
            await Assert.ThrowsAsync<ArgumentException>(() => Service.Get(null, null, Format.Text));
        }

        [Fact]
        public async void TemplateManagerService_Get_NonExistingTemplate_ThrowsNotFoundException()
        {
            Repository.Setup(s => s.Get("nonexisting.template", "1.0", Format.Text)).ReturnsAsync((ITemplate)null);
            await Assert.ThrowsAsync<NotFoundException>(() => Service.Get("nonexisting.template", "1.0", Format.Text));
        }

        [Fact]
        public async void TemplateManagerService_Get_ExistingTemplate_ThrowsArgumentException()
        {
            await Assert.ThrowsAsync<ArgumentException>(() => Service.Get(string.Empty, "1.0", Format.Text));
            await Assert.ThrowsAsync<ArgumentException>(() => Service.Get("test", string.Empty, Format.Text));
            await Assert.ThrowsAsync<ArgumentOutOfRangeException>(() => Service.Get("test.template", "v1.0", (Format)100));
        }

        [Fact]
        public async void TemplateManagerService_Get_ExistingTemplate_ReturnsTemplate()
        {
            var existingTemplate = new Template { Name = "existing.template", Version = "1.0", Body = "test", Format = Format.Text };

            Repository.Setup(s => s.Get("existing.template", "1.0", Format.Text))
                .Returns(() => Task.FromResult<ITemplate>(existingTemplate));

            var template = await Service.Get("existing.template", "1.0", Format.Text);

            Assert.NotNull(template);
        }

        [Fact]
        public async void TemplateManagerService_GetProperties_NonExistingTemplate_ThrowsNotFoundException()
        {
            Repository.Setup(s => s.GetProperties("nonexisting.template", "1.0", Format.Text)).ReturnsAsync((Dictionary<string, object>)null);
            await Assert.ThrowsAsync<NotFoundException>(() => Service.GetProperties("nonexisting.template", "1.0", Format.Text));
        }

        [Fact]
        public async void TemplateManagerService_GetProperties_ExistingTemplate_ThrowsArgumentException()
        {
            await Assert.ThrowsAsync<ArgumentException>(() => Service.GetProperties(string.Empty, "1.0", Format.Text));
            await Assert.ThrowsAsync<ArgumentException>(() => Service.GetProperties("existing.template", string.Empty, Format.Text));
            await Assert.ThrowsAsync<ArgumentOutOfRangeException>(() => Service.GetProperties("test.template", "v1.0", (Format)100));
        }

        [Fact]
        public async void TemplateManagerService_GetProperties_ExistingTemplate_ReturnsTemplateProperties()
        {
            var existingTemplate = new Template
            {
                Name = "existing.template",
                Version = "1.0",
                Body = "test",
                Format = Format.Text,
                Properties = new Dictionary<string, object>()
                {
                         { "pdfSettings", new Dictionary<string,string>(){{ "pageSize", "A4"},{ "orientation", "portrait"}}}
                }
            };

            Repository.Setup(s => s.GetProperties("existing.template", "1.0", Format.Text))
                .Returns(() => Task.FromResult<Dictionary<string, object>>(existingTemplate.Properties));

            var template = await Service.GetProperties("existing.template", "1.0", Format.Text);

            Assert.NotNull(template);
        }

        [Fact]
        public async void TemplateManagerService_GetAll_ReturnsTemplates()
        {
            var existingTemplates = new List<ITemplate>
            {
                new Template {Name = "existing.template", Body = "test", Format = Format.Text},
                new Template {Name = "existing2.template", Body = "test", Format = Format.Text}
            };

            Repository.Setup(s => s.GetAll())
                .Returns(() => Task.FromResult(existingTemplates));

            var templates = await Service.GetAll();

            Assert.NotNull(templates);
            Assert.NotEmpty(templates);
        }

        [Fact]
        public async void TemplateManagerService_Add_ExistingTemplate_ThrowsTemplateAlreadyExist()
        {
            var existingTemplate = new Template { Name = "existing.template", Version = "1.0", Body = "test", Format = Format.Text };

            Repository.Setup(s => s.Get("existing.template", "1.0", Format.Text))
                .Returns(() => Task.FromResult<ITemplate>(existingTemplate));

            await Assert.ThrowsAsync<TemplateAlreadyExists>(() => Service.Add(existingTemplate));
        }

        [Fact]
        public async void TemplateManagerService_Add_NullOrInvalidTemplate_ThrowsException()
        {
            var missingNameTemplate = new Template { Body = "test", Version = "1.0", Format = Format.Text };
            var missingBodyTemplate = new Template { Name = "new.template", Version = "1.0", Format = Format.Text };
            var invalidFormatTemplate = new Template { Name = "new.template", Version = "1.0", Body = "test", Format = (Format)3 };

            await Assert.ThrowsAsync<ArgumentNullException>(() => Service.Add(null));
            await Assert.ThrowsAsync<ArgumentException>(() => Service.Add(missingNameTemplate));
            await Assert.ThrowsAsync<ArgumentException>(() => Service.Add(missingBodyTemplate));
            await Assert.ThrowsAsync<ArgumentOutOfRangeException>(() => Service.Add(invalidFormatTemplate));
        }

        [Fact]
        public async void TemplateManagerService_Add_NewTemplate_ReturnsTemplate()
        {
            var newTemplate = new Template { Name = "new.template", Version = "1.0", Body = "test", Format = Format.Text };

            Repository.Setup(s => s.Add(newTemplate));

            var addedTemplate = await Service.Add(newTemplate);

            Assert.NotNull(addedTemplate);
        }

        [Fact]
        public async void TemplateManagerService_Update_NullOrInvalidTemplate_ThrowsException()
        {
            var missingNameTemplate = new Template { Body = "test", Version = "1.0", Format = Format.Text };
            var missingBodyTemplate = new Template { Name = "new.template", Version = "1.0", Format = Format.Text };
            var invalidFormatTemplate = new Template { Name = "new.template", Version = "1.0", Body = "test", Format = (Format)3 };
            var validFormatTemplate = new Template { Name = "new.template", Version = "1.0", Body = "test", Format = Format.Text };

            await Assert.ThrowsAsync<ArgumentException>(() => Service.Update(It.IsAny<string>(), "v1.0", Format.Text, validFormatTemplate));
            await Assert.ThrowsAsync<ArgumentNullException>(() => Service.Update("test.template", "v1.0", Format.Text, null));
            await Assert.ThrowsAsync<ArgumentException>(() => Service.Update("test.template", "v1.0", Format.Text, missingNameTemplate));
            await Assert.ThrowsAsync<ArgumentException>(() => Service.Update("test.template", "v1.0", Format.Text, missingBodyTemplate));
            await Assert.ThrowsAsync<ArgumentOutOfRangeException>(() => Service.Update("test.template", "v1.0", Format.Text, invalidFormatTemplate));
        }

        [Fact]
        public async void TemplateManagerService_Update_EmptyOrNullTemplateName_ThrowsArgumentException()
        {
            var existingTemplate = new Template { Name = "existing.template", Body = "test", Format = Format.Text };

            await Assert.ThrowsAsync<ArgumentException>(() => Service.Update(null, "v1.0", Format.Text, existingTemplate));
            await Assert.ThrowsAsync<ArgumentException>(() => Service.Update(string.Empty, "v1.0", Format.Text, existingTemplate));
            await Assert.ThrowsAsync<ArgumentException>(() => Service.Update(" ", "v1.0", Format.Text, existingTemplate));
        }

        [Fact]
        public async void TemplateManagerService_Update_ExistingTemplate()
        {
            var existingTemplate = new Template { Name = "existing.template", Version = "v1.0", Body = "test", Format = Format.Text };

            Repository.Setup(s => s.Get("existing.template", "v1.0", Format.Text))
                .Returns(() => Task.FromResult<ITemplate>(existingTemplate));

            Repository.Setup(s => s.Update(existingTemplate));

            await Service.Update("existing.template", "v1.0", Format.Text, existingTemplate);
        }

        [Fact]
        public async void TemplateManagerService_Delete_EmptyOrNullTemplateName_ThrowsArgumentException()
        {
            await Assert.ThrowsAsync<ArgumentException>(() => Service.Delete(null, "v1.0", Format.Text));
            await Assert.ThrowsAsync<ArgumentException>(() => Service.Delete(string.Empty, "v1.0", Format.Text));
            await Assert.ThrowsAsync<ArgumentException>(() => Service.Delete(" ", "v1.0", Format.Text));
            await Assert.ThrowsAsync<ArgumentException>(() => Service.Delete("test.template", string.Empty, Format.Text));
            await Assert.ThrowsAsync<ArgumentOutOfRangeException>(() => Service.Delete("test.template", "v1.0", (Format)100));
        }

        [Fact]
        public async void TemplateManagerService_Delete_ExistingTemplate()
        {
            var existingTemplate = new Template { Name = "existing.template", Body = "test", Format = Format.Text };

            Repository.Setup(s => s.Get("existing.template", "v1.0", Format.Text))
                .Returns(() => Task.FromResult<ITemplate>(existingTemplate));

            Repository.Setup(s => s.Remove(existingTemplate));

            await Service.Delete("existing.template", "v1.0", Format.Text);
        }

        [Fact]
        public async void TemplateManagerService_Delete_NonExistingTemplate_ThrowsNotFoundException()
        {
            Repository.Setup(s => s.Get("nonexisting.template", "v1.0", Format.Text))
                .ThrowsAsync(new NotFoundException("Template not found"));

            await Assert.ThrowsAsync<NotFoundException>(() => Service.Delete("nonexisting.template", "v1.0", Format.Text));
        }

        [Fact(Skip = "Data is not required so test case is not required")]
        public async void TemplateManagerService_Process_NullData_ThrowsArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => Service.Process("existing.template", "1.0", Format.Text, null));
        }

        [Fact]
        public async void TemplateManagerService_Process_EmptyOrNullTemplateName_ThrowsArgumentException()
        {
            await Assert.ThrowsAsync<ArgumentException>(() => Service.Process(null, "v1.0", Format.Text, new { data = "test" }));
            await Assert.ThrowsAsync<ArgumentException>(() => Service.Process(string.Empty, "v1.0", Format.Text, new { data = "test" }));
            await Assert.ThrowsAsync<ArgumentException>(() => Service.Process(" ", "v1.0", Format.Text, new { data = "test" }));
            await Assert.ThrowsAsync<ArgumentException>(() => Service.Process("test.template", string.Empty, Format.Text, new { data = "test" }));
            await Assert.ThrowsAsync<ArgumentOutOfRangeException>(() => Service.Process("test.template", "v1.0", (Format)100, new { data = "test" }));
        }

        [Fact]
        public async void TemplateManagerService_Process_NonExistingTemplate_ThrowsNotFoundException()
        {
            Repository.Setup(s => s.Get("nonexisting.template", "v1.0", Format.Text))
                .ThrowsAsync(new NotFoundException("Template not found"));

            await Assert.ThrowsAsync<NotFoundException>(() => Service.Process("nonexisting.template", "v1.0", Format.Text, new { data = "test" }));
        }

        [Fact]
        public async void TemplateManagerService_Process_ExistingTemplate_ReturnsResult()
        {
            var existingTemplate = new Template { Name = "existing.template", Version = "v1.0", Body = "test", Format = Format.Text,Title="template title" };

            Repository.Setup(s => s.Get("existing.template", "v1.0", Format.Text))
                .Returns(() => Task.FromResult<ITemplate>(existingTemplate));

            Processor.Setup(s => s.Process(It.IsAny<string>(), new { data = "test" }))
                .Returns(() => "test");

            var result = await Service.Process("existing.template", "v1.0", Format.Text, new { data = "test" });
            Assert.NotNull(result);
            Assert.Equal("test", result.Data);
        }

        [Fact]
        public async void TemplateManagerService_Process_ExistingTemplateWithTitle_ReturnsResult()
        {
            var existingTemplate = new Template { Name = "existing.template", Version = "v1.0", Title = "test", Body = "test", Format = Format.Html };

            Repository.Setup(s => s.Get("existing.template", "v1.0", Format.Html))
                .Returns(() => Task.FromResult<ITemplate>(existingTemplate));

            Processor.Setup(s => s.Process(It.IsAny<string>(), new { data = "test" }))
                .Returns(() => "test");

            var result = await Service.Process("existing.template", "v1.0", Format.Html, new { data = "test" });
            Assert.NotNull(result);
            Assert.Equal("test", result.Title);
            Assert.Equal("test", result.Data);
        }
    }
}
