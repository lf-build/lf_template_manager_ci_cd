﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.TemplateManager.Api.Controllers;
using Moq;
using Xunit;
using System;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
namespace LendFoundry.TemplateManager.Api.Tests
{
    public class TemplateManagerControllerTests
    {
        private TemplateManagerController TemplateManagerController { get; set; }

        public TemplateManagerControllerTests()
        {
            var mockTemplateManagerService = new MockTemplateManagerService();
            TemplateManagerController = new TemplateManagerController(mockTemplateManagerService, Mock.Of<ILogger>());
        }

        [Fact]
        public void TemplateManager_Constructor_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new TemplateManagerController(null, Mock.Of<ILogger>()));
        }

        [Fact]
        public async void TemplateManager_Get_WithNoTemplateName_ReturnsAllTemplates()
        {
            var response = (ObjectResult)await TemplateManagerController.Get();
            Assert.Equal(200, response.StatusCode);
        }

        [Fact]
        public async void TemplateManager_Get_WithNonExistingTemplateName_ReturnsErrorResult()
        {
            var response = (ErrorResult)await TemplateManagerController.Get("nonexisting.template", "1.0", Format.Text);
            Assert.Equal(404, response.StatusCode);
        }

        [Fact]
        public async void TemplateManager_GetProperties_WithExistingTemplateName_ReturnsProperties()
        {
            var response = (ObjectResult)await TemplateManagerController.GetProperties("existing.template", "1.0", Format.Text);
            Assert.Equal(200, response.StatusCode);
        }

        [Fact]
        public async void TemplateManager_GetProperties_WithNonExistingTemplateName_ReturnsErrorResult()
        {
            var response = (ErrorResult)await TemplateManagerController.GetProperties("nonexisting.template", "1.0", Format.Text);
            Assert.Equal(404, response.StatusCode);
        }

        [Fact]
        public async void TemplateManager_Get_WithExistingTemplateName_ReturnsObjectResult()
        {
            var response = (ObjectResult)await TemplateManagerController.Get("existing.template", "1.0", Format.Text);
            Assert.Equal(200, response.StatusCode);
        }

        [Fact]
        public async void TemplateManager_AddTemplate_ValidTemplate_ReturnsSuccess()
        {
            var template = new Template { Name = "new.template", Body = "test", Format = Format.Text };
            var response = await TemplateManagerController.AddTemplate(template);
            Assert.Equal(200, ((ObjectResult)response).StatusCode);
        }

        [Fact]
        public async void TemplateManager_AddTemplate_WithExistingTemplate_ReturnsErrorResult()
        {
            var template = new Template { Name = "existing.template", Version = "1.0", Body = "test", Format = Format.Text };
            var response = (ErrorResult)await TemplateManagerController.AddTemplate(template);
            Assert.Equal(400, response.StatusCode);
        }

        [Fact]
        public async void TemplateManager_UpdateTemplate_WithExistingTemplate_ReturnsNoContentResult()
        {
            var template = new Template { Name = "existing.template", Body = "test2", Format = Format.Text };
            var response = (NoContentResult)await TemplateManagerController.UpdateTemplates("existing.template", "1.0", Format.Text, template);
            Assert.Equal(204, response.StatusCode);
        }

        [Fact]
        public async void TemplateManager_UpdateTemplate_WithNonExistingTemplate_ReturnsErrorResult()
        {
            var template = new Template { Name = "nonexisting.template", Body = "test", Format = Format.Text };
            var response = (ErrorResult)await TemplateManagerController.UpdateTemplates("nonexisting.template", "1.0", Format.Text, template);
            Assert.Equal(404, response.StatusCode);
        }

        [Fact]
        public async void TemplateManager_DeleteTemplate_ExistingTemplate_ReturnsNoContentResult()
        {
            var response = (NoContentResult)await TemplateManagerController.DeleteTemplate("existing.template", "1.0", Format.Text);
            Assert.Equal(204, response.StatusCode);
        }

        [Fact]
        public async void TemplateManager_DeleteTemplate_NonExistingTemplate_ReturnsErrorResult()
        {
            var response = (ErrorResult)await TemplateManagerController.DeleteTemplate("nonexisting.template", "1.0", Format.Text);
            Assert.Equal(404, response.StatusCode);
        }

        [Fact]
        public async void TemplateManager_Process_ExistingTemplate_ReturnsSuccess()
        {
            var response = (ObjectResult)await TemplateManagerController.Process("existing.template", "1.0", Format.Text, new { data = "test" });
            Assert.Equal(200, response.StatusCode);
            Assert.NotNull(response.Value);
        }

        [Fact]
        public async void TemplateManager_Process_NonExistingTemplate_ReturnsErrorResult()
        {
            var response = (ErrorResult)await TemplateManagerController.Process("nonexisting.template", "1.0", Format.Text, new { data = "test" });
            Assert.Equal(404, response.StatusCode);
        }
    }
}
