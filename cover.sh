if [ ! -z $1 ]; then
  if [ $1 -lt 0 ] || [ $1 -gt 100 ]; then
    echo "Threshold should be between 0 and 100"
    threshold=80
  fi
  threshold=$1
else
  threshold=80
fi
coverage_folder_path="artifacts/coverage/report"
rm -r -f $coverage_folder_path
mkdir -p $coverage_folder_path

mkdir -p tools
 for project in src/**/*.csproj; do
 dotnet restore --verbosity normal --disable-parallel --ignore-failed-sources --no-cache -s  http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc -s https://api.nuget.org/v3/index.json $project;

 dotnet build --no-restore $project;done

 for project in test/**/*.csproj; do
 dotnet restore --verbosity normal --disable-parallel --ignore-failed-sources --no-cache -s  http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc -s https://api.nuget.org/v3/index.json $project;

dotnet build --no-restore $project;done
cd tools
curl -sSL https://s3-us-west-2.amazonaws.com/lf-devops/build/dotnet2/tools.csproj > tools.csproj
dotnet restore

# Instrument assemblies inside 'test' folder to detect hits for source files inside 'src' folder
dotnet minicover instrument --workdir ../  --assemblies test/**/bin/**/*.dll --exclude-sources src/*Abstractions*/ --exclude-sources src/*Persistence*/ --exclude-sources src/*Client*/*Extensions* --exclude-sources src/*Client*/*Factory* --exclude-sources src/*Api*/Startup.cs --exclude-sources src/*Api*/Settings.cs --exclude-sources src/*Api*/Program.cs --sources src/**/*.cs --hits-file $coverage_folder_path/coverage-hits.txt --coverage-file $coverage_folder_path/coverage.json

# Reset hits count in case minicover was run for this project
dotnet minicover reset

cd ..

for project in test/**/*.csproj; do

 dotnet test  --no-build  $project; done

cd tools

# Uninstrument assemblies, it's important if you're going to publish or deploy build outputs
dotnet minicover uninstrument --workdir ../ --coverage-file $coverage_folder_path/coverage.json

# Create HTML reports inside folder coverage-html
# This command returns failure if the coverage is lower than the threshold
dotnet minicover htmlreport --workdir ../ --coverage-file $coverage_folder_path/coverage.json --output $coverage_folder_path/coverage_report --threshold $threshold

# Print console report
# This command returns failure if the coverage is lower than the threshold
dotnet minicover report --workdir ../ --coverage-file $coverage_folder_path/coverage.json --threshold $threshold

# Create NCover report
dotnet minicover xmlreport --workdir ../ --coverage-file $coverage_folder_path/coverage.json --output $coverage_folder_path/coverage.xml --threshold $threshold

cd ..