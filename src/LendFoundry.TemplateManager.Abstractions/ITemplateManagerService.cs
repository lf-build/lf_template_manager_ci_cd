﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.TemplateManager
{
    public interface ITemplateManagerService
    {
        Task<ITemplate> Get(string templateName, string version, Format format);
        Task<Dictionary<string, object>> GetProperties(string templateName, string version, Format format);
        Task<List<ITemplate>> GetAll();
        Task<ITemplate> Add(ITemplate template);
        Task Update(string templateName, string version, Format format, ITemplate template);
        Task Delete(string templateName, string version, Format format);
        Task<ITemplateResult> Process(string templateName, string version, Format format, object data);
    }
}
