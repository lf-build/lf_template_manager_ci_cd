﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.TemplateManager
{
    public interface IHandlebarsResolver
    {
        void Start();
    }
}
