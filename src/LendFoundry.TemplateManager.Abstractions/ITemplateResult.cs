namespace LendFoundry.TemplateManager
{
    public interface ITemplateResult
    {
        string Title { get; set; }
        string Data { get; set; }
    }
}