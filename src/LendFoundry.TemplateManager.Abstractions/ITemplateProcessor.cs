﻿namespace LendFoundry.TemplateManager
{
    public interface ITemplateProcessor
    {
        string Process(string body, object data);
    }
}