﻿namespace LendFoundry.TemplateManager.Events
{
    public class TemplateProcessed
    {
        public ITemplate Template { get; set; }
        public object Data { get; set; } 
    }
}