﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.TemplateManager
{
    public interface ITemplateManagerRepository : IRepository<ITemplate>
    {
        Task<List<ITemplate>> GetAll();
        Task<ITemplate> Get(string templateName, string version, Format format);
        Task<Dictionary<string, object>> GetProperties(string templateName, string version, Format format);
    }
}
