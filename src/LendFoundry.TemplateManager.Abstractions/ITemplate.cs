﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.TemplateManager
{        
    public interface ITemplate : IAggregate
    {
        string Name { get; set; }
        string Title { get; set; }
        Format Format { get; set; }
        string Body { get; set; }
        string Version { get; set; }
        Dictionary<string, object> Properties { get; set; }
    }
}
