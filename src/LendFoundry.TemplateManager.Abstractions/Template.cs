﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.TemplateManager
{
    public class Template : Aggregate, ITemplate
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public Format Format { get; set; }
        public string Body { get; set; }
        public string Version { get; set; }
        public Dictionary<string, object> Properties { get; set; }
    }
}
