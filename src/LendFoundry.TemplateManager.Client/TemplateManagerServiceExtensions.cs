using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.TemplateManager.Client
{
    public static class TemplateManagerServiceExtensions
    {
        public static IServiceCollection AddTemplateManagerService(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddTransient<ITemplateManagerServiceFactory>(p => new TemplateManagerServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ITemplateManagerServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}