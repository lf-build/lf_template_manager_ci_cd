﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using RestSharp;

namespace LendFoundry.TemplateManager.Client
{
    public class TemplateManagerService : ITemplateManagerService
    {
        public TemplateManagerService(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task<ITemplate> Get(string templateName, string version, Format format)
        {
            if (string.IsNullOrWhiteSpace(templateName))
                throw new ArgumentException("Argument is null or whitespace", nameof(templateName));

            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentException("Argument is null or whitespace", nameof(version));

            if (!Enum.IsDefined(typeof(Format), format))
                throw new ArgumentOutOfRangeException(nameof(format));

            var request = new RestRequest("/{templateName}/{version}/{format}", Method.GET);
            request.AddUrlSegment("templateName", templateName);
            request.AddUrlSegment("version", version);
            request.AddUrlSegment("format", format.ToString());
            var result = await Client.ExecuteAsync<Template>(request);
            return result;
        }

        public async Task<Dictionary<string,object>> GetProperties(string templateName, string version, Format format)
        {
            if (string.IsNullOrWhiteSpace(templateName))
                throw new ArgumentException("Argument is null or whitespace", nameof(templateName));

            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentException("Argument is null or whitespace", nameof(version));

            if (!Enum.IsDefined(typeof(Format), format))
                throw new ArgumentOutOfRangeException(nameof(format));

            var request = new RestRequest("/{templateName}/{version}/{format}/properties", Method.GET);
            request.AddUrlSegment("templateName", templateName);
            request.AddUrlSegment("version", version);
            request.AddUrlSegment("format", format.ToString());
            var result = await Client.ExecuteAsync<Dictionary<string,object>>(request);
            return result;
        }

        public async Task<List<ITemplate>> GetAll()
        {
            var request = new RestRequest("/", Method.GET);
            var result = await Client.ExecuteAsync<List<Template>>(request);
            return new List<ITemplate>(result);
        }

        public async Task<ITemplate> Add(ITemplate template)
        {
            var request = new RestRequest("/", Method.POST);
            var json = JsonConvert.SerializeObject(template);
            request.AddParameter("application/json", json, ParameterType.RequestBody);
            var result = await Client.ExecuteAsync<Template>(request);
            return result;
        }

        public async Task Update(string templateName, string version, Format format, ITemplate template)
        {
            if (string.IsNullOrWhiteSpace(templateName))
                throw new ArgumentException("Argument is null or whitespace", nameof(templateName));

            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentException("Argument is null or whitespace", nameof(version));

            if (!Enum.IsDefined(typeof(Format), format))
                throw new ArgumentOutOfRangeException(nameof(format));

            var request = new RestRequest("/{templateName}/{version}/{format}", Method.PUT);
            request.AddUrlSegment("templateName", templateName);
            request.AddUrlSegment("version", version);
            request.AddUrlSegment("format", format.ToString());
            var json = JsonConvert.SerializeObject(template);
            request.AddParameter("application/json", json, ParameterType.RequestBody);
            await Client.ExecuteAsync(request);
        }

        public async Task Delete(string templateName, string version, Format format)
        {
            if (string.IsNullOrWhiteSpace(templateName))
                throw new ArgumentException("Argument is null or whitespace", nameof(templateName));

            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentException("Argument is null or whitespace", nameof(version));

            if (!Enum.IsDefined(typeof(Format), format))
                throw new ArgumentOutOfRangeException(nameof(format));

            var request = new RestRequest("/{templateName}/{version}/{format}", Method.DELETE);
            request.AddUrlSegment("templateName", templateName);
            request.AddUrlSegment("version", version);
            request.AddUrlSegment("format", format.ToString());
            await Client.ExecuteAsync(request);
        }


        public async Task<ITemplateResult> Process(string templateName, string version, Format format, object data)
        {
            if (string.IsNullOrWhiteSpace(templateName))
                throw new ArgumentException("Argument is null or whitespace", nameof(templateName));

            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentException("Argument is null or whitespace", nameof(version));

            if (!Enum.IsDefined(typeof(Format), format))
                throw new ArgumentOutOfRangeException(nameof(format));

            var request = new RestRequest("{templateName}/{version}/{format}/process", Method.POST);
            request.AddUrlSegment("templateName", templateName);
            request.AddUrlSegment("version", version);
            request.AddUrlSegment("format", format.ToString());
            var json = JsonConvert.SerializeObject(data);
            request.AddParameter("application/json", json, ParameterType.RequestBody);
            var result = await Client.ExecuteAsync<TemplateResult>(request);
            return result;
        }

    }

}
