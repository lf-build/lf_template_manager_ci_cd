using System;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;

namespace LendFoundry.TemplateManager.Client
{
    public class TemplateManagerServiceFactory : ITemplateManagerServiceFactory
    {
        public TemplateManagerServiceFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }

        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public ITemplateManagerService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new TemplateManagerService(client);
        }
    }
}