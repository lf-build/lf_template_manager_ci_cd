﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;

using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
namespace LendFoundry.TemplateManager.Api.Controllers
{
    [Route("/")]
    public class TemplateManagerController : ExtendedController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TemplateManagerController"/> class.
        /// </summary>
        /// <param name="templateManagerService">The template manager service.</param>
        /// <param name="logger">The logger.</param>
        /// <exception cref="System.ArgumentNullException">templateManagerService</exception>
        public TemplateManagerController(ITemplateManagerService templateManagerService, ILogger logger) : base(logger)
        {
            if (templateManagerService == null)
                throw new ArgumentNullException(nameof(templateManagerService));

            TemplateManagerService = templateManagerService;
        }

        private ITemplateManagerService TemplateManagerService { get; }

        private static readonly NoContentResult NoContentResult = new NoContentResult();

        /// <summary>
        /// Gets the specified template name.
        /// </summary>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="version">The version.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        [HttpGet("{templateName}/{version}/{format}")]
        [ProducesResponseType(typeof(ITemplate), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> Get(string templateName, string version, Format format)
        {
            return await ExecuteAsync(async () =>
            {
                templateName = WebUtility.UrlDecode(templateName);
                version = WebUtility.UrlDecode(version);
                return Ok(await TemplateManagerService.Get(templateName, version, format));
            });
        }

        /// <summary>
        /// Gets the properties.
        /// </summary>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="version">The version.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        [HttpGet("{templateName}/{version}/{format}/properties")]
        [ProducesResponseType(typeof(IDictionary<string, object>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetProperties(string templateName, string version, Format format)
        {
            return await ExecuteAsync(async () =>
            {
                templateName = WebUtility.UrlDecode(templateName);
                version = WebUtility.UrlDecode(version);
                return Ok(await TemplateManagerService.GetProperties(templateName, version, format));
            });
        }

        /// <summary>
        /// Gets this instance.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(ITemplate[]), 200)]
        public Task<IActionResult> Get()
        {
            return ExecuteAsync(async () => Ok(await TemplateManagerService.GetAll()));
        }

        /// <summary>
        /// Adds the template.
        /// </summary>
        /// <param name="template">The template.</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ITemplate), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddTemplate([FromBody]Template template)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await TemplateManagerService.Add(template));
                }
                catch (TemplateAlreadyExists ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// Updates the templates.
        /// </summary>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="version">The version.</param>
        /// <param name="format">The format.</param>
        /// <param name="template">The template.</param>
        /// <returns></returns>
        [HttpPut("{templateName}/{version}/{format}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> UpdateTemplates(string templateName, string version, Format format, [FromBody] Template template)
        {
            return await ExecuteAsync(async () =>
            {
                templateName = WebUtility.UrlDecode(templateName);
                version = WebUtility.UrlDecode(version);
                await TemplateManagerService.Update(templateName, version, format, template);
                return NoContentResult;
            });
        }

        /// <summary>
        /// Deletes the template.
        /// </summary>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="version">The version.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        [HttpDelete("{templateName}/{version}/{format}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> DeleteTemplate(string templateName, string version, Format format)
        {
            return await ExecuteAsync(async () =>
            {
                templateName = WebUtility.UrlDecode(templateName);
                version = WebUtility.UrlDecode(version);
                await TemplateManagerService.Delete(templateName, version, format);
                return NoContentResult;
            });
        }

        /// <summary>
        /// Processes the specified template name.
        /// </summary>
        /// <param name="templateName">Name of the template.</param>
        /// <param name="version">The version.</param>
        /// <param name="format">The format.</param>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        [HttpPost("{templateName}/{version}/{format}/process")]
        [ProducesResponseType(typeof(ITemplateResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> Process(string templateName, string version, Format format, [FromBody] object data)
        {
            return await ExecuteAsync(async () =>
            {
                templateName = WebUtility.UrlDecode(templateName);
                version = WebUtility.UrlDecode(version);
                return Ok(await TemplateManagerService.Process(templateName, version, format, data));
            });
        }
    }
}