﻿#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.TemplateManager
{
    public static class HandlebarsExtendedMapExtensions
    {
        public static void UseHandlebars(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IHandlebarsResolver>().Start();
        }
    }
}
