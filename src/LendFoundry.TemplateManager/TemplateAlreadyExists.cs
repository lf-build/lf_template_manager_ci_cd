﻿using System;
using System.Runtime.Serialization;

namespace LendFoundry.TemplateManager
{
    [Serializable]
    public class TemplateAlreadyExists : Exception
    {
        public TemplateAlreadyExists()
        {
        }

        public TemplateAlreadyExists(string message) : base(message)
        {
        }

        public TemplateAlreadyExists(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected TemplateAlreadyExists(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}