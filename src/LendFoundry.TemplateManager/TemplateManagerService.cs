﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Services;

namespace LendFoundry.TemplateManager
{
    public class TemplateManagerService : ITemplateManagerService
    {
        public TemplateManagerService(ITemplateManagerRepository templateRepository, ITemplateProcessor templateProcessor, IEventHubClient eventHub)
        {
            if (templateRepository == null)
                throw new ArgumentNullException(nameof(templateRepository));
            if (eventHub == null)
                throw new ArgumentNullException(nameof(eventHub));

            TemplateRepository = templateRepository;
            TemplateProcessor = templateProcessor;
            EventHub = eventHub;
        }

        private ITemplateManagerRepository TemplateRepository { get; }
        private ITemplateProcessor TemplateProcessor { get;}
        private IEventHubClient EventHub { get; }

        public async Task<ITemplate> Get(string templateName, string version, Format format)
        {
            if (string.IsNullOrWhiteSpace(templateName))
                throw new ArgumentException("Argument is null or whitespace", nameof(templateName));

            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentException("Argument is null or whitespace", nameof(version));

            if (!Enum.IsDefined(typeof (Format), format))
                throw new ArgumentOutOfRangeException(nameof(format));

            var template=  await TemplateRepository.Get(templateName,version,format);
            if(template==null)
                throw new NotFoundException("Template not found");
            return template;
        }

        public async Task<Dictionary<string, object>> GetProperties(string templateName, string version, Format format)
        {
            if (string.IsNullOrWhiteSpace(templateName))
                throw new ArgumentException("Argument is null or whitespace", nameof(templateName));

            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentException("Argument is null or whitespace", nameof(version));

            if (!Enum.IsDefined(typeof(Format), format))
                throw new ArgumentOutOfRangeException(nameof(format));

            var properties = await TemplateRepository.GetProperties(templateName, version, format);
            if (properties == null)
                throw new NotFoundException("Template not found");
            return properties;
        }

        public Task<List<ITemplate>> GetAll()
        {
            return TemplateRepository.GetAll();
        }

        public async Task<ITemplate> Add(ITemplate template)
        {
            EnsureTemplateIsValid(template);

            if (await TemplateRepository.Get(template.Name,template.Version,template.Format) != null)
                throw new TemplateAlreadyExists("Template with same name is already exist");
            
            TemplateRepository.Add(template);

            await EventHub.Publish(new Events.TemplateAdded
            {
                Template = template
            });

            return template;
        }


        public async Task Update(string templateName, string version, Format format, ITemplate template)
        {
            EnsureTemplateIsValid(template);

            if (string.IsNullOrWhiteSpace(templateName))
                throw new ArgumentException("Argument is null or whitespace", nameof(templateName));

            var existingTemplate = await Get(templateName,version,format);
            existingTemplate.Name = template.Name;
            existingTemplate.Title = template.Title;
            existingTemplate.Body = template.Body;
            existingTemplate.Format = template.Format;
            existingTemplate.Properties = template.Properties;

            TemplateRepository.Update(existingTemplate);

            await EventHub.Publish(new Events.TemplateUpdated
            {
                Template = template
            });
        }

        private static void EnsureTemplateIsValid(ITemplate template)
        {
            if (template == null)
                throw new ArgumentNullException(nameof(template));

            if (string.IsNullOrWhiteSpace(template.Name))
                throw new ArgumentException("Template name is required", nameof(template.Name));

            if (string.IsNullOrWhiteSpace(template.Body))
                throw new ArgumentException("Template body is required", nameof(template.Body));

            if (string.IsNullOrWhiteSpace(template.Version))
                throw new ArgumentException("Argument is null or whitespace", nameof(template.Version));

            if (!Enum.IsDefined(typeof(Format), template.Format))
                throw new ArgumentOutOfRangeException(nameof(template.Format));
        }

        public async Task Delete(string templateName, string version, Format format)
        {
            if (string.IsNullOrWhiteSpace(templateName))
                throw new ArgumentException("Argument is null or whitespace", nameof(templateName));

            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentException("Argument is null or whitespace", nameof(version));

            if (!Enum.IsDefined(typeof(Format), format))
                throw new ArgumentOutOfRangeException(nameof(format));

            var existingTemplate =  await Get(templateName,version,format);

            TemplateRepository.Remove(existingTemplate);

            await  EventHub.Publish(new Events.TemplateRemoved
            {
                Template = existingTemplate
            });
        }

        public async Task<ITemplateResult> Process(string templateName, string version, Format format, object data)
        {

            if (string.IsNullOrWhiteSpace(templateName))
                throw new ArgumentException("Argument is null or whitespace", nameof(templateName));

            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentException("Argument is null or whitespace", nameof(version));

            if (!Enum.IsDefined(typeof(Format), format))
                throw new ArgumentOutOfRangeException(nameof(format));

            var template = await Get(templateName, version, format);

            var titleResult = string.Empty;

            if (!string.IsNullOrWhiteSpace(template.Title))
            {
                titleResult = TemplateProcessor.Process(template.Title, data);
            }

            var bodyResult = TemplateProcessor.Process(template.Body, data);

            EventHub.Publish(new Events.TemplateProcessed
            {
                Template = template
            });

            return new TemplateResult { Title = titleResult, Data = bodyResult };
        }
    }
}
